<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.html'?>
        <div class="form-box">
            <div class="login-container" id="login">
                <form action="verif_login.php" method="GET">
                    <div class="top">
                        <header>Se connecter</header>
                        <span>Vous ne possedez pas de compte ?<a href="register.php" onclick="register()">S'inscrire</a></span>
                        <?php
                            if(isset($_SESSION['Pseudo'])){
                                echo "<div style=\"color: rgb(255, 50, 50)\">
                                    <b style=\"color: rgb(255, 0, 0)\">ATTENTION !</b>
                                    Le mot de passe de " . $_SESSION['Pseudo'] . " est incorrect, veuillez réessayer.
                                    </div>";
                            }
                            if(isset($_SESSION['error_msg'])){
                                echo "<div style=\"color: rgb(255, 50, 50)\">" . $_SESSION['error_msg'] . "</div>";
                            }
                            session_unset();// remove session variables => remove $_SESSION['Pseudo']
                        ?>
                    </div>
                    <div class="input-box">
                        <input type="text" name="Pseudo" class="input-field" width="40%" placeholder="Pseudo">
                        <i class="bx bx-user"></i>
                    </div>
                    <div class="input-box">
                        <input type="password" name="Password" class="input-field" placeholder="Mot de Passe">
                        <i class="bx bx-lock-alt"></i>
                    </div>
                    <div class="input-box">
                        <input type="submit" class="submit" value="Connexion">
                    </div>
                    <div class="two-col">
                        <div class="one">
                            <input type="checkbox" id="login-check">
                            <label for="login-check">Se souvenir</label>
                        </div>
                        <div class="two">
                            <label><a href="#">Mot de Passe oublié ?</a></label>
                        </div>
                    </div>
                </form>
            </div>
    </div>

<script>
    var a = document.getElementById("loginBtn");
    var b = document.getElementById("registerBtn");
    var x = document.getElementById("login");
    var y = document.getElementById("register");

    function login() {
        x.style.left = "4px";
        y.style.right = "-520px";
        a.className += " white-btn";
        b.className = "btn";
        x.style.opacity = 1;
        y.style.opacity = 0;
    }
</script>

</body>
</html>
