-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 17 mai 2024 à 11:15
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cy_love_database`
--

-- --------------------------------------------------------

--
-- Structure de la table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Pseudo` varchar(100) DEFAULT NULL,
  `Mot_de_passe` varchar(50) DEFAULT NULL,
  `Prénom` varchar(100) DEFAULT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user_info`
--

INSERT INTO `user_info` (`ID`, `Pseudo`, `Mot_de_passe`, `Prénom`, `Nom`, `Email`) VALUES
(1, 'PS1', 'MDP1', 'PRENOM1', 'NOM1', 'MAIL1'),
(2, 'PS2', 'MDP2', 'PRENOM2', 'NOM2', 'MAIL2'),
(3, 'PS3', 'MDP3', 'PRENOM3', 'NOM3', 'MAIL3'),
(4, 'PS4', 'MDP4', 'PRENOM4', 'NOM4', 'MAIL4'),
(5, 'PS5', 'MDP5', 'PRENOM5', 'NOM5', 'MAIL5'),
(6, 'PS6', 'MDP6', 'PRENOM6', 'NOM6', 'MAIL6'),
(7, 'flo_dessin', 'chiasse2.0', 'Flora', 'Ryka', 'juli_2.0@hotmailwiskicaca.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
