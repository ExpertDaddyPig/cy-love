<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>Récupération de mot de passe</title>
</head>

<body style="background-image: url('https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000')">
    <div class="wrapper">
        <?php include 'header.html'?>
        <div style="text-align: left; color: white">
            <?php
                // Connexion à la base de données
                $servername = "localhost";
                $login = "root";
                $pass = "";

                try {
                    $connexion = new PDO("mysql:host=$servername;dbname=cylove", $login, $pass);
                    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    // Vérification des données envoyées par le formulaire
                    if(isset($_POST['pseudo']) && isset($_POST['email'])) {
                        $pseudo = $_POST['pseudo'];
                        $email = $_POST['email'];

                        // Vérification de l'existence du pseudo et de l'email dans la base de données
                        $query = $connexion->prepare("SELECT Mot_de_passe FROM user_info WHERE Pseudo = :pseudo AND Email = :email");
                        $query->bindParam(':pseudo', $pseudo);
                        $query->bindParam(':email', $email);
                        $query->execute();
                        $result = $query->fetch(PDO::FETCH_ASSOC);

                        if($result) {
                            $mot_de_passe = $result['Mot_de_passe'];
                            echo "Votre mot de passe est : $mot_de_passe";
                        } else {
                            echo "Les informations fournies ne correspondent à aucun compte.";
                        }
                    }
                } catch(PDOException $e) {
                    echo "Connexion impossible : " . $e->getMessage();
                }
            ?>
        </div>
        <div>
            <h2>Récupération de mot de passe</h2>
            <form method="post" action="" class="login-form">
      <div class="input-box">
          <label for="pseudo">Pseudo:</label>
          <input type="text" id="pseudo" name="pseudo" class="input-field" required>
      </div>
      <div class="input-box">
          <label for="email">Email:</label>
          <input type="email" id="email" name="email" class="input-field" required>
      </div>
      <div class="input-box">
          <input type="submit" class="submit" value="Récupérer le mot de passe">
      </div>
  </form>
</div>
  </div>
</body>
</html>
