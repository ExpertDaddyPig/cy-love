<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.html'?>
        <div style="text-align: left; color: white">
            <?php
                //Add account in cy_love_database
                $servername = "localhost";
                $login = "root";
                $pass = "";
                
                //server connexion test
                try{    
                    $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
                    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode
                    //echo "Connexion à la base de données réussie";
                    
                    $new_Pseudo = $_GET["Pseudo"];
                    $new_Password =  $_GET["Password"];
                    $new_Firstname = $_GET["Firstname"];
                    $new_Name = $_GET["Name"];
                    $new_Email = $_GET["Email"];
                    $ID = $_SESSION["ID"];

                    //get all info account
                    $query_info_account = $connexion->prepare("SELECT * FROM user_info WHERE ID = '$ID'");
                    $query_info_account->execute();
                    $array_info_account = $query_info_account->fetchall(PDO::FETCH_NUM); // array with all info account
                    echo "<p style='color: white; width: 30%;'>";
                    echo "count array_info_account : " . count($array_info_account[0]) . "<br>";
                    print_r($array_info_account[0]);
                    for ($i=0; $i<count($array_info_account[0]); $i++){
                        echo "<br>" . $i . "  " . $array_info_account[0][$i];
                    }
                    echo "</p><br>";

                    if( strlen($new_Password)<=0 || strlen($new_Pseudo)<=0 || strlen($new_Email)<=0 || strlen($new_Firstname)<=0 || strlen($new_Name)<=0 ){
                        $_SESSION['error_msg'] = "Aucun champ ne doit être vide !";
                        header("Location: personal-account.php");
                        exit;
                    }

                    //TEST for MODIFY PSEUDO (or not)
                    $query1 = $connexion->prepare(//query = requête
                        "SELECT ID, Pseudo FROM user_info WHERE Pseudo = '$new_Pseudo'"
                    );
                    $query1->execute();
                    $array_same_pseudo = $query1->fetchall(PDO::FETCH_NUM); // array in array with same pseudos as the new (and their associated ID)
                    echo "count(array_same_pseudo) : " . count($array_same_pseudo) . "<br>";
                    print_r($array_same_pseudo);
                    //test how many identical pseudosas the new are in the database
                    if( count($array_same_pseudo) >= 2){
                        $_SESSION['error_msg'] = " ERREUR ETRANGE n°1000.<br>Le pseudo " . $new_Pseudo . " est déjà pris par AU MOINS un autre utilisateur !<br>Veuillez en choisir un autre.";
                        //header("Location: personal-account.php");
                        print_r($_SESSION);
                        exit;
                    }
                    elseif( count($array_same_pseudo) == 1 ){
                        if( $array_same_pseudo[0][0] == $ID){
                            //we meet the user connected pseudo, this pseudo does not belong (n'appartient pas) to someone else
                            //pseudo not changed
                            echo "Pseudo non changé";
                        }
                        else{
                            $_SESSION['error_msg'] = "Le pseudo " . $new_Pseudo . " est déjà pris par un autre utilisateur !<br>Veuillez en choisir un autre";
                            //header("Location: personal-account.php");
                            print_r($_SESSION);
                            exit;
                        }

                    }
                    // NEW PSEUDO correct (not belong to someone else)
                    else{               //count($array_same_pseudo) == 0
                        //change user pseudo
                        $query_pseudo_update = $connexion->prepare(
                            "UPDATE user_info
                            SET Pseudo = '$new_Pseudo'
                            WHERE ID = '$ID';"
                        );
                        $query_pseudo_update->execute();
                        $_SESSION['Pseudo'] = $new_Pseudo;
                        echo "<br>Votre pseudo a été mis à jour.";
                    }
                    //MODIFY INFO ACCOUNT
                    $query2 = $connexion->prepare(
                        "UPDATE user_info
                        SET Mot_de_passe = '$new_Password', Prénom = '$new_Firstname', Nom = '$new_Name', Email = '$new_Email'
                        WHERE ID = '$ID';"
                    );
                    $query2->execute();
                    echo "Vos informations ont bien été mises à jour.";
                }

                catch (PDOException $e){
                    echo "Connexion impossible : " . $e->getMessage();
                }
            ?>
        </div>
    </div>
</body>
</html>