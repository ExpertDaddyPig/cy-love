<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style4.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.html'?>
        <div class="register-container" id="register">
            <form action="verif_register.php" method="GET">
                <div class="top">
                    <header>S'inscrire</header>    
                    <span>J'ai déja un compte ?<a href="login.php" onclick="login()">Se connecter</a></span>
                        
                    <?php
                        if(isset($_SESSION['Pseudo'])){
                            echo "<div style=\"color: rgb(255, 50, 50)\">
                                <b style=\"color: rgb(255, 0, 0)\">ATTENTION !</b>
                                Le pseudo " . $_SESSION['Pseudo'] . " est déjà utilisé par un autre utilisateur, veuillez en saisir un autre.
                                </div>";
                        }
                        if(isset($_SESSION['error_msg'])){
                            echo "<div style=\"color: rgb(255, 50, 50)\">" . $_SESSION['error_msg'] . "</div>";
                        }
                        session_unset();// remove session variables => remove $_SESSION['Pseudo']
                    ?>
                </div>
                <div class="input-box">
                    <input type="text" name="Firstname" class="input-field" placeholder="Prénom">
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="text" name="Name" class="input-field" placeholder="Nom">
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="text" name="Email" class="input-field" placeholder="Email">
                    <i class="bx bx-envelope"></i>
                </div>
                <div class="input-box">
                    <input type="text" name="Pseudo" class="input-field" placeholder="Pseudo">
                    <i class="bx bx-user"></i>
                </div>
                <div class="input-box">
                    <input type="password" name="Password" class="input-field" placeholder="Mot de Passe">
                    <i class="bx bx-lock-alt"></i> <!-- In "Boxicons" library, don't show the password-->
                </div>
                <div class="input-box">
                    <input type="submit" class="submit" value="Valider">
                </div>
                <div class="two-col">
                    <div class="one">
                        <input type="checkbox" name="register-check" id="register-check">
                        <label for="register-check">Se souvenir</label>
                    </div>
                    <div class="two">
                        <label><a href="#">Termes & conditions</a></label>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script>
   function register() {
        var x = document.getElementById("login");
        var y = document.getElementById("register");

        x.style.left = "-510px";
        y.style.right = "5px";
        x.style.opacity = 0;
        y.style.opacity = 1;
   }
</script>

</body>
</html>
