<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000')">
    <div class="wrapper">
        <?php include 'header.html'?>
        <div style="text-align: left; color: white">
            <?php
                //Add account in cy_love_database
                $servername = "localhost";
                $login = "root";
                $pass = "";

                //server connexion test
                try{
                    $connexion = new PDO("mysql:host=$servername;dbname=cylove", $login, $pass);
                    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode
                    //echo "Connexion à la base de données réussie";

                    $Pseudo = $_GET["Pseudo"];
                    $Password =  $_GET["Password"];
                    if( strlen($Password)<=0 || strlen($Pseudo)<=0 ){
                        $_SESSION['error_msg'] = "Aucun champ ne doit être vide !";
                        header("Location: login.php");
                        exit;
                    }

                    //TEST if the pseudo exists in database
                    $query_all_pseudos = $connexion->prepare(//query = requête
                        "SELECT Pseudo FROM user_info"
                    );
                    $query_all_pseudos->execute();
                    $array_all_pseudos = $query_all_pseudos->fetchall(); // array with all pseudos in database
                    for ($i=0; $i<count($array_all_pseudos); $i++){
                        if ($Pseudo == $array_all_pseudos[$i][0]){
                            $query_pwd = $connexion->prepare(
                                "SELECT Mot_de_passe
                                FROM user_info
                                WHERE Pseudo = '$Pseudo';"
                            );
                            $query_pwd->execute();
                            $result_pwd = $query_pwd->fetchall(); // array with the correct password

                            $_SESSION['Pseudo'] = $Pseudo;
                            if($Password == $result_pwd[0][0]){
                                echo "Connexion réussie.";
                                echo "<br>Bienvenue " . $Pseudo . "<br>Mot de passe : " . $Password;
                            }
                            else{
                                header("Location: login.php");
                                exit;
                            }
                        }
                    }
                }

                catch (PDOException $e){
                    echo "Connexion impossible : " . $e->getMessage();
                }
            ?>
        </div>
        <h1>Recherche de profils</h1>
<form action="search.php" method="GET" class="form-container">
    <div class="input-box">
        <input type="text" id="keyword" name="keyword" class="input-field" placeholder="Mot-clé">
        <button type="submit" class="submit">
        <span>Rechercher</span>
        <i class="bx bx-search"></i>
      </button>
    </div>
</form>

    </div>
</body>
</html>
