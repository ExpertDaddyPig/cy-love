<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style4.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.html'?>
        <div style="text-align: left; color: white">
            <?php
                //Add account in cy_love_database
                $servername = "localhost";
                $login = "root";
                $pass = "";
                
                //server connexion test
                try{    
                    $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
                    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode
                    //echo "Connexion à la base de données réussie";
                    
                    $Pseudo = $_GET["Pseudo"];
                    $Password =  $_GET["Password"];
                    $Firstname = $_GET["Firstname"];
                    $Name = $_GET["Name"];
                    $Email = $_GET["Email"];
                    if( strlen($Password)<=0 || strlen($Pseudo)<=0 || strlen($Email)<=0 || strlen($Firstname)<=0 || strlen($Name)<=0 ){
                        $_SESSION['error_msg'] = "Aucun champ ne doit être vide !";
                        header("Location: register.php");
                        exit;
                    }

                    //TEST if the pseudo exists in database
                    $query1 = $connexion->prepare(//query = requête
                        "SELECT Pseudo FROM user_info"
                    );
                    $query1->execute();
                    $result = $query1->fetchall(); // array with all pseudos in database
                    for ($i=0; $i<count($result); $i++){
                        if ($Pseudo == $result[$i][0]){
                            $_SESSION['Pseudo'] = $Pseudo;
                            header("Location: register.php");
                            exit;
                        }
                    }

                    //CREATE ACCOUNT
                    $query = $connexion->prepare(
                        "INSERT INTO user_info (Pseudo, Mot_de_passe, Prénom, Nom, Email)
                        Values(:pseudo, :pwd, :firstname, :lastname, :email)"
                    );
                    /*OU $sql = "INSERT INTO user_info (ID, Mot_de_passe, Prénom, Nom, Email)
                            Values('$ID', '$Password', '$Firstname', '$Name', '$Email')";
                    $connexion->exec($sql); */
                    
                    $query->bindParam(':pseudo', $Pseudo);//bind = lier
                    $query->bindParam(':pwd', $Password);
                    $query->bindParam(':firstname', $Firstname);
                    $query->bindParam(':lastname', $Name);
                    $query->bindParam(':email', $Email);

                    $query->execute();
                    echo "<br>Nouveau compte créé";
                }

                catch (PDOException $e){
                    echo "Connexion impossible : " . $e->getMessage();
                }
            ?>
        </div>
    </div>
</body>
</html>