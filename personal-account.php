<?php
    session_start();
    $_SESSION['is_connected'] = 'oui';
    $_SESSION['Pseudo'] = 'PS1';
    $Pseudo = $_SESSION['Pseudo'];

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('Images/Background_images.jpg')"> <!--Background image : https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000-->
    <div class="wrapper">
        <?php include 'header.html'?>
        <?php
            $servername = "localhost";
            $login = "root";
            $pass = "";
            
            if( isset($_SESSION['Pseudo']) && isset($_SESSION['is_connected']) && $_SESSION['is_connected']=='oui' ){
                //affiche les infos de compte
            }
            else{
                echo "<p>Vous n'êtes pas connecté(e) à votre session.<br>
                    Connectez vous en cliquant <a href='login.php'>ici</a>.</p>";
                exit;
            }

            //server connexion test
            try{
                $connexion = new PDO("mysql:host=$servername;dbname=cy_love_database", $login, $pass);
                $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //PDO error mode

                $query_info_account = $connexion->prepare("SELECT * FROM user_info WHERE Pseudo = '$Pseudo'");
                $query_info_account->execute();
                $array_info_account = $query_info_account->fetchall(PDO::FETCH_NUM); // array with all info of the user
                echo "<p style='color: white; width: 30%;'>";
                print_r($array_info_account[0]);
                for ($i=0; $i<count($array_info_account[0]); $i++){
                    echo "<br>" . $i . "  " . $array_info_account[0][$i];
                }
                echo "</p>";

                
                $ID = $array_info_account[0][0];
                $_SESSION['ID'] = $ID;
                echo "ID : " . $ID;
            }
            catch (PDOException $e){
                echo "Connexion impossible à la base de données: " . $e->getMessage();
                exit;
            }
        ?>
        <fieldset style="border: 0;"> <!--fieldset is used to group related element in a form-->
            <header>Vos informations <?php echo $_SESSION['Pseudo'];?></header>
            <?php
                print_r($_SESSION);
                if(isset($_SESSION['error_msg'])){
                    echo "<div style=\"color: rgb(255, 50, 50)\">" . $_SESSION['error_msg'] . "</div>";
                }
                unset($_SESSION['error_msg']);// remove only this session variable
            ?>
            <form action="register_modification_account.php" method="GET">
                <table style="width: 500px; background-color: yellow; text-align: left;">
                    <tr>
                        <th>
                            <label for="gender">Genre</label> <!-- label: pour que label soit utile, ce qu'il y a dans 'for' doit être associé à l'id dans le input qui suit, ici 'for' pas très utile-->
                        </th>
                        <td>
                            <input type="radio" name="gender" value="Madame">Madame
                            <input type="radio" name="gender" value="Monsieur">Monsieur
                            <input type="radio" name="gender" value="Non binaire">Non binaire
                            <input type="radio" name="gender" value="Non défini">Non défini
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="Pseudo">Pseudo</label>
                        </th>
                        <td>
                            <input type="text" name="Pseudo" id="Pseudo" size="12" value="<?php echo $array_info_account[0][1]?>" >
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            <label for="Password">Mot de passe</label>
                        </th>
                        <td>
                            <input type="text" name="Password" id="Password" size="12" value="<?php echo $array_info_account[0][2]?>" >
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="Firstname">Prénom</label>
                        </th>
                        <td>
                            <input type="text" name="Firstname" id="Firstname" size="20" value="<?php echo $array_info_account[0][3]?>">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="Name">Nom</label>
                        </th>
                        <td>
                            <input type="text" name="Name" id="Name" size="20" value="<?php echo $array_info_account[0][4]?>">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="Email">Email</label>
                        </th>
                        <td>
                            <input type="text" name="Email" id="Email" size="20" value="<?php echo $array_info_account[0][5]?>">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="Profession">Profession</label>
                        </th>
                        <td>
                            <input type="text" name="Profession" id="Profession" size="20">
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <th>
                            <label for="address">Adresse</label>
                        </th>
                        <td>
                            <textarea name="address" rows="3" cols="30" value="test">Rue No Boîte postale</textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="adress-code">No postal/Localité</label>
                        </th>
                        <td>
                            <input type="text" maxlength="4" size="4">
                            <input type="text" size="30" value="Lausanne">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="country">Pays</label>
                        </th>
                        <td>
                            <select name="country" style="background-color: white;">
                                <option value="england">Angleterre</option>
                                <option value="germany">Allemagne</option>
                                <option value="australia">Australie</option>
                                <option value="france">France</option>
                                <option value="swiss" selected>Suisse</option>
                                <option value="italy">Italie</option>
                                <option value="japon">Japon</option>
                                <option value="usa">USA</option>
                                <option value="other">Autre</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="platforms">Plateforme(s)</label>
                        </th>
                        <td>
                            <input type="checkbox" value="windows" id="windows"><label for="windows">Windows</label>
                            <input type="checkbox" value="macintosh" id="macintosh"><label for="macintosh">Macintosh</label>
                            <input type="checkbox" value="unix" id="unix"><label for="unix">Unix</label>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label for="apps">Application(s)</label>
                        </th>
                        <td>
                            <select name="apps" size="5" multiple style="background-color: white;">
                                <option value="office">Bureautique</option>
                                <option value="dao">DAO</option>
                                <option value="statistics">Statistiques</option>
                                <option value="sgbd">SGBD</option>
                                <option value="internet">Internet</option>
                            </select>
                        </td>
                    </tr>
                    -->
                </table>
                <input type="submit" value="Enregistrer">
            </form>
        </fieldset>
    </div>
</body>
</html>

