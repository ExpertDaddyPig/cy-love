<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style2.css">
    <title>CY LOVE</title>
</head>

<body style="background-image: url('https://img.freepik.com/photos-gratuite/jeune-couple-romantique-sexy-amoureux-heureux-plage-ete-ensemble-s-amusant-portant-maillots-bain-montrant-signe-du-coeur-sundet_285396-6545.jpg?t=st=1715103572~exp=1715107172~hmac=144c7e5b0ff875c6caeab703b9f2860b0da711ca04f6eb9e9186eb8b7e9f819d&w=2000')">
    <div class="wrapper">
        <?php include 'header.html'?>
        <?php
// Vérifier si le mot-clé de recherche est présent
if(isset($_GET['keyword'])) {
    // Connexion à la base de données
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "cylove";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Récupération du mot-clé de recherche
        $keyword = $_GET['keyword'];

        // Requête SQL pour rechercher dans la table user_info
        $sql = "SELECT Pseudo, Prénom, nom FROM user_info WHERE Prénom LIKE :keyword OR nom LIKE :keyword";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':keyword', "%$keyword%", PDO::PARAM_STR);
        $stmt->execute();

        // Vérifier s'il y a des résultats
        if ($stmt->rowCount() > 0) {
            // Affichage des profils correspondants
            while ($row = $stmt->fetch()) {
                echo "Prénom: " . $row["Prénom"]. " - Nom: " . $row["nom"]. "<br>";
                // Afficher la photo si elle existe
                //if (!empty($row["photo"])) {
                //    echo "<img src='" . $row["photo"] . "' alt='Photo de profil'><br>";
                //} else {
                //    echo "Pas de photo de profil disponible<br>";
              //  }
            }
        }
else {
            echo "Aucun résultat trouvé";
        }
    } catch(PDOException $e) {
        echo "Erreur de connexion à la base de données: " . $e->getMessage();
    }
} else {
    echo "Veuillez entrer un mot-clé de recherche.";
}
?>
<h1>Recherche de profils</h1>
<form action="search.php" method="GET" class="form-container">
<div class="input-box">
<input type="text" id="keyword" name="keyword" class="input-field" placeholder="Mot-clé">
<button type="submit" class="submit">
<span>Rechercher</span>
<i class="bx bx-search"></i>
</button>
</div>
</form>
</body>
</html>
